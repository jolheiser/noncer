module gitea.com/jolheiser/noncer

go 1.17

require (
	github.com/BrianLeishman/go-imap v0.0.0-20220307142709-356e9ab3bc77
	github.com/JohannesKaufmann/html-to-markdown v1.3.3
	github.com/adrg/xdg v0.4.0
	github.com/caarlos0/log v0.1.10
	github.com/peterbourgon/ff/v3 v3.3.0
)

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/StirlingMarketingGroup/go-retry v0.0.0-20190512160921-94a8eb23e893 // indirect
	github.com/andybalholm/cascadia v1.1.0 // indirect
	github.com/aymanbagabas/go-osc52 v1.2.1 // indirect
	github.com/cention-sany/utf7 v0.0.0-20170124080048-26cad61bd60a // indirect
	github.com/charmbracelet/lipgloss v0.6.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28 // indirect
	github.com/jaytaylor/html2text v0.0.0-20200412013138-3577fbdbcff7 // indirect
	github.com/jhillyerd/enmime v0.9.3 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.13.0 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.4.2 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	golang.org/x/net v0.0.0-20210501142056-aec3718b3fa0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.3.6 // indirect
)
